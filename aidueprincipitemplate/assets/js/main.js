jQuery(document).ready(function(){
    initForm();
    loadImagesLL();
    initMenu();
    initSliders();
    initGallery();
    fixHeaderScroll();
    fixFade();
});


jQuery( window ).on('load', function() {
    playVideo();
    setTimeout(function(){ 
        jQuery('body').addClass('loaded');
    }, 1000);
});

jQuery(window).scroll(function(){
    fixHeaderScroll();
    fixFade();
});

jQuery( window ).resize(function() {
    loadImagesLL();
});

function initLoad() {
    jQuery(body).addClass('loaded');
}

function loadImagesLL() {
    if( jQuery( window ).width() > 767 ) {
        jQuery('*[data-ll]').each(function( index ) {
            jQuery(this).attr('style', 'background-image: url(' + jQuery(this).attr('data-ll') + ')');
        });
    }
}

function playAudio() {
	if( jQuery('#headerVideo').length > 0 ) {
		jQuery( '.header-slider__audio i' ).toggleClass( "fa-volume-off fa-volume-up" );
       	jQuery('#headerVideo').prop('muted', !jQuery('#headerVideo').prop('muted'));
	}
}

function playVideo() {
    if( jQuery('#headerVideo').length > 0 ) {
        jQuery('#headerVideo').get(0).play();
        if(jQuery('#headerVideo').get(0).paused) {
            jQuery('.header-slider__play').css('display', 'block');
        } else {
            jQuery('.header-slider__play').css('display', 'none');
        }
    }
}

function fixBanner() {
    jQuery('.header-banner__top').parent().width(jQuery('.header-banner__top').first().width());
    jQuery('.header-banner__bottom').css('display', 'block');
}

function fixFade() {
    jQuery('.fade').each(function( index ) {
        fadeCurrentPosition = (jQuery(this).offset().top ) - ( jQuery( window ).scrollTop() + ( jQuery(window).height() ) );
        if( fadeCurrentPosition < 0 ) {
            jQuery(this).addClass('fade-active');
        }  
    });

    jQuery('.abs, .page-block__parallax').each(function( index ) {
        fadeCurrentPosition = (jQuery(this).offset().top ) - ( jQuery( window ).scrollTop() + ( jQuery(window).height() ) );
        if( fadeCurrentPosition < 0 ) {
            jQuery(this).addClass('active');
        }  
    });
}

function initMenu() {
    jQuery('.header-hamburger').click(function(event) {
        if( jQuery('.hamburger', this).hasClass('is-active') ) {
            jQuery('.hamburger', this).removeClass('is-active');
            jQuery('body').removeClass('menu');
        } else {
            jQuery('.hamburger', this).addClass('is-active');
            jQuery('body').addClass('menu');
        }
    });
        
    jQuery('li .smtoggler').click(function(event) {
        var submenu = jQuery(this).closest('li');
        var close = submenu.hasClass('active');
        jQuery('.header-menu li.active').removeClass('active');;
        if( close ) {
            submenu.removeClass('active');
        } else {
            submenu.addClass('active');
        }
    });
    
    jQuery('.header-bar__sub span').click(function(event) {
    	jQuery('.header-hamburger .hamburger').trigger( "click" );
    });
}

function fixHeaderScroll() {
    if( jQuery( window ).scrollTop() > 150 ) {
        jQuery('body').addClass('scrolled');
    } else {
        jQuery('body').removeClass('scrolled');
    }
}

function initSliders() {
    jQuery('.header-slider__skip').click(function(event) {
        jQuery('html, body').animate({
            scrollTop: jQuery( '.header-slider' ).first().height() - ( jQuery( '.header-bar' ).first().outerHeight() + 32 )
        }, 1000);
    });
    jQuery('.header-slider__slides').each(function( index ) {
        jQuery(this).slick({
            dots: true,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 7000,
            pauseOnHover: false,
            appendDots: jQuery('.page-slider-nav', jQuery(this).parent()),
            fade: true,
            cssEase: 'linear',
        });
    });
    jQuery('.page-offers__slider').each(function( index ) {
        jQuery(this).slick({
            dots: false,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 5000,
            pauseOnHover: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            cssEase: 'linear',
            responsive: [
            	{
            		breakpoint: 1024,
            		settings: {
            			slidesToShow: 3,	
            		}	
                },
                {
            		breakpoint: 768,
            		settings: {
            			slidesToShow: 2,	
            		}	
            	},
                {
            		breakpoint: 480,
            		settings: {
            			slidesToShow: 1,	
            		}	
            	}
            ]
        });
    });
}

function initForm() {
    jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ calendarLanguage ] );
    jQuery('#a_date, #p_date').datepicker({
        dateFormat: "dd-mm-yy",
        minDate: new Date(),
        onSelect: function(dateText) {
            calendarDateChange();
        }
    });
    populateFields();
}

function calendarDateChange() {
    var notti = calcola_notti(jQuery("#a_date").val(), jQuery("#p_date").val());
    if( notti < 1 ) {
        jQuery("#notti_1").val('1');
        jQuery("#p_date").val(nextDay(jQuery("#a_date").val()));   
    } else {
        jQuery("#notti_1").val(notti);
    }
    
    populateFields();
}

function populateFields() {
    if( jQuery("#a_date").val().length >= 10 ) {
        var date = jQuery("#a_date").val();
        jQuery("#a_gg").html(date.substring(0, 2));
        jQuery("#a_mm").html(jQuery.datepicker.formatDate('M', new Date( parseInt(date.substring(6, 10)), parseInt(date.substring(3, 5))-1, parseInt(date.substring(0, 2)) )));
        //jQuery("#a_aa").html(date.substring(6, 10));

        jQuery("#gg").val(date.substring(0, 2));
        jQuery("#mm").val(date.substring(3, 5));
        jQuery("#aa").val(date.substring(6, 10));
    }
}

function nextDay(startDate) {
    startDate = startDate.split('-');
    var anno_i = parseInt(startDate[2]);
    var mese_i = parseInt(startDate[1]);
    var giorno_i = parseInt(startDate[0]);
    var newDate = new Date(anno_i, mese_i - 1, giorno_i);
    newDate.setTime( newDate.getTime() + 86400000 );
    anno_i = newDate.getFullYear();
    mese_i = newDate.getMonth() + 1  < 10 ? '0' + (newDate.getMonth() + 1) : (newDate.getMonth() + 1);
    giorno_i = newDate.getDate() < 10 ? '0' + newDate.getDate() : newDate.getDate();
    return giorno_i + '-' + mese_i + '-' + anno_i;
}

function calcola_notti(alternate_data_inizio, alternate_data_fine) {

    alternate_data_inizio = alternate_data_inizio.split('-');
    var anno_i = parseInt(alternate_data_inizio[2]);
    var mese_i = parseInt(alternate_data_inizio[1]);
    var giorno_i = parseInt(alternate_data_inizio[0]);

    alternate_data_fine = alternate_data_fine.split('-');
    var anno_p = parseInt(alternate_data_fine[2]);
    var mese_p = parseInt(alternate_data_fine[1]);
    var giorno_p = parseInt(alternate_data_fine[0]);

    fromdate = new Date(anno_i, mese_i - 1, giorno_i);
    todate = new Date(anno_p, mese_p - 1, giorno_p);
    
    var giorni_differenza = (todate - fromdate) / 86400000;
    giorni_differenza = Math.round(giorni_differenza);
    
    return giorni_differenza;

}

function submitBookingForm() {
    if( jQuery('#form_qr').length > 0 ) {
        jQuery('#submitReal').trigger( "click" );    
    }
}

function initGallery() {
    jQuery( '.swipebox, .swipebox-video' ).swipebox();
}

function filterGallery(categoryID) {
    jQuery('div[data-cat^="cat-"]').css('display', 'none');
    if( categoryID == '0' ) {
        jQuery('div[data-cat^="cat-"]').css('display', 'inline-block');
    } else {
        jQuery('div[data-cat="cat-' + categoryID + '"]').css('display', 'inline-block');
    }
    jQuery('a[data-filter^="fil-"]').removeClass('active');
    jQuery('a[data-filter="fil-' + categoryID + '"]').addClass('active');
}

function triggerClick(elementID) {
	jQuery('#' + elementID).trigger( "click" );
}